class Test<T>{
    T obj;
    Test(T obj){
        this.obj = obj;
    }

    public T getObj() {
        return this.obj;
    }
}

class First{

    public static void main(String[] args){

        //Creating a String Generics
        Test <String> sObj = new Test<String>("Chiranjeeb");
        System.out.println(sObj.getObj());

        //Creating a Integer Generics
        Test <Integer> iObj = new Test<Integer>(10);
        System.out.println(iObj.getObj());

        //Creating a Float Generics
        Test<Double> fObj = new Test<Double>(23.5);
        System.out.println(fObj.getObj());

    }

}
