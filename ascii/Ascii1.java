public class Ascii1{
    public static void main(String[] args){
        char ch1 = 'a';
        char ch2 = 'Q';
        char ch3 = '5'; //53
        String str = "5";
        int i = 5;
        System.out.println(ch3==i);

        int asciiValue1 = ch1;
        int asciiValue2 = ch2;

        System.out.println("ASCII value of " + ch1 + " is " + asciiValue1);
        System.out.println("ASCII value of " + ch2 + " is " + asciiValue2);
    }
}

//
