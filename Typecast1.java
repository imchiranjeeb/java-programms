//Widening Casting
public class Typecast1{
    public static void main(String[] args){
        int num = 9;
        double dubNum = num; //Int-Double
        float f2 = num; //Int-Float
        String str = "23";
        int i = Integer.parseInt(str); // String to Int
        float f = Float.parseFloat("200"); //String to Float
        double d = Double.parseDouble("100"); // String to Double
        //Print Statements
        System.out.println(num);
        System.out.println(dubNum);
        System.out.println(i); // String to Int
        System.out.println(f); // String to Float
        System.out.println(d); // String to Double 
        //How to Know Variable Datatype
        //1. For Primitive Types
        System.out.println(((Object)d).getClass().getSimpleName()); 
        System.out.println(((Object)f).getClass().getSimpleName());
        //2.For Non-Primitive Types
        System.out.println(str.getClass().getSimpleName());
        //Int-String / Float-String / Double-String
        int is = 19; //is=Integer-String
        float fs = 25.45f; //fs=Float-String
        double ds = 45.132; //ds = Double-String
        String res1 = Integer.toString(is); // Int-String
        String res2 = Float.toString(fs); // Float-String
        String res3 = Double.toString(ds); // Double-String
        System.out.println(res1);
        System.out.println(res2);
        System.out.println(res3);
    }
}
