// Online Java Compiler
// Use this editor to write, compile and run your Java code online

class A {
    
    public void m1(int i,float j){
        System.out.println("i is : " + i + " & j is : " + j);
    }
    
    public void m1(float i,int j){
        System.out.println("i is : " + i + " & j is : " + j);
    }
    
    public void m1(int i,int j){
        System.out.println("i is : " + i + " & j is : " + j);
    }
    
    public void m1(float i,float j){
        System.out.println("i is : " + i + " & j is : " + j);
    }
    
    public void m1(String i){
        System.out.println("My Name is : " + i);
    }
    
}


class First {
    public static void main(String[] args) {
         A a = new A();
         a.m1(12,13.2f);
         a.m1(12.f,14);
         a.m1(12.f,13.6f);
         a.m1("Rahul");
         a.m1(12,13);

         A a1 = new A();
         a1.m1("Chandan");
    }
}