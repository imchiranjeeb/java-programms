class A {
    public float sum(float i,float j){
        float sum = i + j;
        return sum;
    }
}
class B extends A {
    public int multi(int i,int j){
        return i*j;
    }
}
class C extends B{
    public int sqr(int a){
        return a*a;
    }
}

class D extends C{}

class Multilevel {
    public static void main(String[] args) {
         D ccc = new D();
         int result = ccc.multi(4,5);
        //  System.out.println(result);
         
         float result2 = ccc.sum(3.2f,2.2f);
         System.out.println(result2);
         
    }
}