class A {
    public float sum(float i,float j){
        float sum = i + j;
        return sum;
    }
}
class B extends A {
    public int multi(int i,int j){
        return i*j;
    }
}
class C extends A{
    public int sqr(int a){
        return a*a;
    }
}
class Hierarchial {
    public static void main(String[] args) {
         C ccc = new C();
         int result = ccc.multi(4,5);
         System.out.println(result);
         
         float result2 = ccc.sum(3.2f,2.2f);
         System.out.println(result2);
         
    }
}