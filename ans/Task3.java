public class Task3{
    public static void main(String[] args){
        int math=78, english=85, history=64, science=60, gk=70;
        int avg = (math+english+history+science+gk)/5;
        String grade;
        System.out.println(avg);

        if(avg>=90){
            grade = "a++;";
        }else if(avg>=80 && avg<90){
            grade = "a";
        }else if(avg>=70 && avg<80){
            grade = "b++";
        }else if(avg>=60 && avg<70){
            grade = "b";
        }else if(avg>=50 && avg<60){
            grade = "c";
        }else if(avg>=40 && avg<50){
            grade = "d";
        }else{
            grade = "F";
        }

        System.out.println("You have secured " + grade + " grade.");
    }
}