public class Task5{
    public static void main(String[] args){
        int i = -6;
        int result;
        if(i<0){
            result = Math.abs(i);
        }else if(i>0){
            result = i;
        }else{
            result = 0;
        }
        System.out.println(result);
    }
}