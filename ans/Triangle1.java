import java.util.Scanner;
public class Triangle1{
    public static void main(String[] args){
        Scanner obj = new Scanner(System.in);
        System.out.println("Enter 3 angles values : ");

        int angle1,angle2,angle3,sum;

        angle1 = obj.nextInt();
        angle2 = obj.nextInt();
        angle3 = obj.nextInt();

        sum = angle1+angle2+angle3;

        if (sum==180){
            if(angle1>0 && angle2>0 && angle3 > 0){
                System.out.println("Triangle is Valid.");
            }else{
                System.out.println("Sum of angles is 180 but Triangle is not Valid.");
            }
        }else{
            System.out.println("Triangle is not Valid.");
        }
    }
}
