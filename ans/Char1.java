import java.util.Scanner;
public class Char1{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        char c = sc.next().charAt(0);

        if(c>='A' && c<='Z'){
            System.out.println("This is  Capital Letter.");
        }else if(c>='a' && c<='z'){
            System.out.println("This is a Small Letter.");
        }else{
            System.out.println("This is not an Alphabet.");
        }

    }
}
