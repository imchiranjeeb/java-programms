import java.util.Scanner;
public class Char2{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        char c = sc.next().charAt(0);

        if( (c>='A' && c<='Z') || (c>='a' && c<='z')){
            System.out.println("This is an Alphabet.");
        }else if((c>='0' && c<='9')){
            System.out.println("This is a Digit.");
        }else{
            System.out.println("This is a Special Character.");
        }
    }
}
