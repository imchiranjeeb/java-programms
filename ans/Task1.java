public class Task1{
    public static void main(String[] args){
        String month = "june";
        String season;

        if (month.equals("october") || month.equals("november") || month.equals("december") || month.equals("january")){
            //System.out.println("Winter Season");
            season = "Winter Season";
        }else if(month.equals("february") || month.equals("march") || month.equals("april") || month.equals("may")){
            season = "Summer Season";
        }else if(month.equals("june") || month.equals("july") || month.equals("august") || month.equals("september")){
            season = "Rainy Season";
        }else{
            season = "Not Found";
        }

        System.out.println("This is " + season + ".");
    }
}