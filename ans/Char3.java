import java.util.Scanner;
public class Char3{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        char ch = sc.next().charAt(0);

        if(ch=='A' || ch=='E' || ch=='I' || ch=='O' || ch=='U' || ch=='a' || ch=='e' || ch=='i' || ch=='o' || ch=='u'){
            System.out.println("This is a Vowel.");
        }else if((ch>='A' && ch<='Z') || (ch>='a' && ch<='z')){
            System.out.println("This is Consonant");
        }else{
            System.out.println("This is not an alphabet.");
        }
    }
}
