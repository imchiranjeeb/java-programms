//Java Scanner class allows the user to take input from the console.
// import java.util.*;
import java.util.Scanner;
public class Input{
    public static void main(String[] args){
        int i;
        float f;
        double d;
        String s;
        boolean b;
        char c;
        Scanner sc = new Scanner(System.in);

        //Integer Input
        System.out.print("Enter the value of I : ");
        i = sc.nextInt();
        System.out.println(i);

        //Float Input
        System.out.print("Enter the value of F : ");
        f = sc.nextFloat();
        System.out.println(f);

        //Double Input
        System.out.print("Enter the value of D : ");
        d = sc.nextDouble();
        System.out.println(d);

        //String Input
        System.out.print("Enter the value of S : ");
        s = sc.nextLine();
        System.out.println(s);

        //Boolean Input
        System.out.print("Enter the value of B : ");
        b = sc.nextBoolean();
        System.out.println(b);

        //Character Input
        System.out.print("Enter the value of C : ");
        c = sc.next().charAt(0); //chfb
        System.out.println(c);
    }
}