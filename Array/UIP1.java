import java.util.Scanner;
public class UIP1{
    public static void main(String[] args){
        Scanner ob = new Scanner(System.in);
        int m;
        System.out.print("Enter How Many Elements You Want to Enter in the Array : ");
        m = ob.nextInt();
        int[] numbers = new int[m];

        System.out.println("Enter Elements One By One Inside Array : ");
        for(int i=0; i<m; i++){
            numbers[i] = ob.nextInt();
        }

        System.out.println("Array Elements are Here : ");
        for(int i=0; i<m; i++){
            System.out.println(numbers[i]);
        }
    }
}