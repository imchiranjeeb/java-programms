//An array in C/C++ or be it in any programming language is a collection of similar data items stored in a single variable
public class Second{
    public static void main(String[] args){
        int m = 5;
        String[] fruits = new String[m];

        fruits[0]="mango";
        fruits[1]="lemon";
        fruits[2]="banana";
        fruits[3]="orange";

        for (int j=0; j<m; j++) {
            System.out.println(fruits[j]);
        }
    }
}
