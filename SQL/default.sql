CREATE table person(
    id int not null auto_increment primary key,
    name varchar(255),
    email varchar(255) not null,
    address varchar(255) default 'not provided',
    unique(email)
);

-- insert to table
insert into person(name, email, address) values ('looser2','looser2@looser.com','bhuban'),('looser3','looser3@looser.com','bhubaneswar'),('looser4','looser4@looser.com','pallahra');

select * from person where email regexp "2";


alter table person add phone varchar(255) after email;
alter table person add college varchar(255);


update person set phone = "7894362773" where id = 3;
update person set college = "trident" where id = 5;

-- change table name
alter table person rename to persons;

-- to remove a column
alter table persons drop column college;

-- to delete a ROW
delete from persons where id = 5;

-- to delete all the rows from a table
truncate table persons;