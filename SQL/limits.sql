CREATE table employee(
    id int not null auto_increment primary key,
    name varchar(255),
    email varchar(255) not null,
    salary int,
    unique(email)
);

-- limit clause
select * from employee limit 3;

-- min && max clause
select min(salary) from employee;
select max(salary) from employee;

-- where clause
select name from employee where salary > 20000;

-- count clause
select count(id) from  employee where salary > 20000;

-- avg clause
select avg(salary) from employee where salary > 20000;

-- sum clause
select sum(salary) from employee;

-- in operator
select name,email from employee where address in("bbsr","bhuban");

-- between operator
select name,email from employee where salary between 10000 and 20000;


-- Check
CREATE TABLE Persons (
    Slno int NOT NULL auto_increment primary key,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Age int,
    CHECK (Age>=18)
);

ALTER TABLE Persons ADD CHECK (Age>=18);