select if(A+B<=C or B+C<=A or A+C<=B,"Not A Triangle",if(A=B and B=C,"Equilateral",if(A=B or B=C or A=C,"Isosceles","Scalene")))
from TRIANGLES as T;

select IF(A=B AND B=C,"Equilateral",IF(A+B<=C OR B+C<=A OR A+C<=B,"Not A Triangle",IF(A=B OR B=C OR A=C,"Isosceles","Scalene"))) FROM TRIANGLEs;

select IF(A+B<=C OR B+C<=A OR A+C<=B,"Not A Triangle",IF(A=B AND B=C,"Equilateral",IF(A=B OR B=C OR A=C,"Isosceles","Scalene"))) FROM TRIANGLEs;


-- the-pads problem
SELECT CONCAT(Name,"(",substr(Occupation,1,1),")") FROM OCCUPATIONS ORDER BY NAME ASC;
SELECT "There are a total of ",COUNT(Occupation),CONCAT(lower(Occupation),"s.") FROM OCCUPATIONS GROUP BY Occupation ORDER BY COUNT(Occupation),Occupation ASC;

--BST Problem

SELECT N, IF(P IS NULL,"Root",IF((SELECT COUNT(*) FROM BST WHERE P=B.N)>0,"Inner","Leaf")) FROM BST AS B ORDER BY N;


-- The Blunder
SELECT CEIL(AVG(Salary)-AVG(REPLACE(Salary,'0',''))) FROM EMPLOYEES;


--Weather Observation 17
SELECT ROUND(LONG_W,4) FROM STATION WHERE LAT_N = (SELECT MIN(LAT_N) FROM STATION WHERE LAT_N > 38.7780);
