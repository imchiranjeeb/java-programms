
CREATE TABLE Persons (
    ID int NOT NULL,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Age int,
    PRIMARY KEY (ID)
);

insert into Persons (ID,LastName,FirstName,Age) values (1,"Sahoo","Chandan",24),(2,"Pattnaik","Prabin",25),(3,"Parida","Manisha",24),(4,"Sethy","Rahul",24);


CREATE TABLE Orders (
    OrderID int NOT NULL,
    OrderNumber int NOT NULL,
    PersonID int,
    PRIMARY KEY (OrderID),
    FOREIGN KEY (PersonID) REFERENCES Persons(ID)
);


insert into Orders (OrderID, OrderNumber, PersonID) values (3329,56,2),(3330,54,3);