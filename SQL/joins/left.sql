
-- Query-1
-- Here Persons is the left table
select Persons.FirstName,Persons.Age,Orders.OrderID,Orders.OrderNumber FROM Persons LEFT JOIN Orders ON Orders.PersonID = Persons.ID;

-- Query-2
-- Here Orders is the left table
select Orders.OrderID,Orders.OrderNumber,Persons.FirstName,Persons.Age FROM Orders LEFT JOIN Persons ON Persons.ID = Orders.PersonID;
