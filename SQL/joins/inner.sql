
-- Query-1

select * from Persons INNER JOIN Orders ON Persons.ID = Orders.PersonID;

-- Query-2

select Orders.OrderID,Orders.OrderNumber,Persons.FirstName,Persons.Age FROM Orders INNER JOIN Persons ON Orders.PersonID = Persons.ID;

select Orders.OrderID,Orders.OrderNumber,Persons.FirstName,Persons.Age FROM Persons INNER JOIN Orders ON Orders.PersonID = Persons.ID;