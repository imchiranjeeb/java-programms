

-- Query-1
SELECT * FROM Persons CROSS JOIN Orders;


-- Query-2
SELECT Persons.FirstName,Orders.OrderID FROM Persons CROSS JOIN Orders;


-- Query-3
SELECT Persons.FirstName,Orders.OrderID FROM Persons CROSS JOIN Orders WHERE Orders.PersonID = Persons.ID;;
