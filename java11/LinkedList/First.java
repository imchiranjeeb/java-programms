import java.util.LinkedList;
import java.util.ArrayList;


public class First {
   
    public static void main(String[] args){

        LinkedList<String> nameLinkedList = new LinkedList<>();
        ArrayList<String> nameArrayList = new ArrayList<>();


        //Adding Value to nameLinkedList
        nameLinkedList.add("Chiranjeeb");
        nameLinkedList.add("Goldy");
        nameLinkedList.add("Chandan");

        //Adding Value to nameArrayList
        nameArrayList.add("Chiranjeeb");
        nameArrayList.add("Goldy");
        nameArrayList.add("Chandan");


        for(int i=0;i<nameLinkedList.size(); i++){
            System.out.print(nameLinkedList.get(i) + "\n");
        }

        for(String listElement : nameArrayList){
            System.out.println(listElement + "\n");
        }
    }
}
