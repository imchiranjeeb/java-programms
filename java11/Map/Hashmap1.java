import java.util.HashMap;


public class Hashmap1{
    public static void main(String[] args){

        HashMap<String,Integer> empId = new HashMap<>();

        empId.put("Chiru",123);
        empId.put("Looser",456);
        empId.put("Chiranjeeb",789);

        System.out.println(empId);
        System.out.println("Chirus id is : "+empId.get("Chiru"));
        System.out.println("-----------------------------------");
        empId.put("Chandan",256);
        System.out.println("Size of the Map is : "+empId.size());

        System.out.println("------------------------------------");

        //Print Keys of Map
        for(String i:empId.keySet()){
            System.out.println(i);
        }
        System.out.println("------------------------------------");
        // Print Values
        for(Integer i:empId.values()){
            System.out.println(i);
        }
        //Print Both Key && Values
        for(String i:empId.keySet()){
            System.out.println("Key : " + i + " && " + " Value is : " + empId.get(i));
        }

        //Check if a key is present
        System.out.println(empId.containsKey("Chiru"));
        //Check if a value is present
        System.out.println(empId.containsValue(567));
        //Changing Value of a map
        empId.put("Chiru",143);
        System.out.println(empId);
        //Add key value if not present
        empId.putIfAbsent("Goldy", 555);
        System.out.println(empId);


    }
}