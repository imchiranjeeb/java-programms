public class Quick {

    public static int partition(int arr[],int low,int high){

        int pivot = arr[high];
        int i = low-1;

        for(int j=low; j<high; j++){
            
            if(arr[j] < pivot){
                i++;
                //swap
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }

        i++;
        int temp = arr[i];
        arr[i] = pivot;
        arr[high] = temp;

        return i; //pivot index
    }

    public static void quickSort(int arr[], int low, int high) {

        if(low < high) {

            int pid = partition(arr, low, high);

            quickSort(arr,low,pid-1);
            quickSort(arr,pid+1,high);
        }
    }

    public static void main(String[] args){

        int[] arr = {7,3,4,9,1,6,2,8,5};
        int arrLength = arr.length;

        quickSort(arr, 0, arrLength-1);

        for(int i=0; i<arrLength; i++){
            System.out.println(arr[i]+" ");
        }

        System.out.println();

    }
}