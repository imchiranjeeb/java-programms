public class First {
    public static void main(String[] args){
        try {
            int[] nums = {5,2,3,7};

            int arrayLength = nums.length;

            for(int i=0; i<=arrayLength; i++) {
                System.out.println(nums[i]);
            }

        }catch(Exception e){
            System.out.println("Something Went Wrong");
            System.out.println(e);
        }finally{
            System.out.println("Done With Try..Catch");
        }
    }
}
