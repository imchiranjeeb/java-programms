
abstract class Parents{

    public Parents(){
        System.out.println("Parent class Constructor");
    }

    public void sayHello(){
        System.out.println("Hello.");
    }

    abstract void greet();

}

class Child extends Parents{

    public void homeWork(){
        System.out.println("Do Home Work.");
    }

    @Override
    public void greet(){
        System.out.println("Say Good Morning.");
    }
}


class First{
    
    public static void main(String[] args){

        Child c = new Child();
        c.greet();
    }
}