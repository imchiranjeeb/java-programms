import java.util.*;



public class First {

    public static void main(String[] args){

        StringBuilder sb = new StringBuilder("Goldi");


        System.out.println(sb);
        System.out.println(sb.charAt(0));
        //replace i->y
        sb.setCharAt(sb.length() -1, 'y');
        System.out.println(sb);

        //insert character without removing anything
        sb.insert(0,'L');
        System.out.println(sb);

        //delete char with index
        sb.delete(0,1);
        System.out.println(sb);

        //to add a character at the end
        sb.append('y');
        System.out.println(sb);
    }
    
}
