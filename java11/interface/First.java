
interface BiCycle {

    int applyBreak(int currentSpeed, int decrementSpeed);
    int speedUp(int currentSpeed, int incrementSpeed);

}

class HeroCycle implements BiCycle{

    int maxSpeed;
    String color;

    public HeroCycle(int cycleMaxSpeed,String cycleColour){
        this.maxSpeed = cycleMaxSpeed;
        this.color = cycleColour;
    }

    public int maxSpeed(){
        return this.maxSpeed;
    }

    public String colour(){
        return this.color;
    }

    public int applyBreak(int currentSpeed, int decrementSpeed){

        if( currentSpeed > this.maxSpeed || decrementSpeed > this.maxSpeed ) return 0;
        if(currentSpeed - decrementSpeed < 0) return 0;
        
        return currentSpeed - decrementSpeed;
    }

    public int speedUp(int currentSpeed, int incrementSpeed){

        return currentSpeed + incrementSpeed;
    }
}

public class First {
    public static void main(String[] args){

        HeroCycle hero = new HeroCycle(70,"Red");

        String cycleColour = hero.colour();
        int speed = hero.maxSpeed();
        int afterBreakSpeed = hero.applyBreak(70,20);
        int afterSpeed = hero.speedUp(20,25);

        System.out.println(cycleColour);
        System.out.println(speed);
        System.out.println(afterBreakSpeed);
        System.out.println(afterSpeed);
    }
}
