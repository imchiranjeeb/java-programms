
class A{

    public int sum(int x, int y){
        return (x+y);
    }

    public int multi(int x, int y){
        return (x*y);
    }
}


class B extends A{

    public double sum(double x, double y,double z){
        return (x+y+z);
    }

    public int multi(int x, int y, int z){
        return (x*y*z);
    }

}



public class Overload {
    public static void main(String[] args) {
        
        B b = new B();
        
        int sum1 = b.sum(5, 6);
        double sum2 = b.sum(5.6,7.32,4.5);

        int multi1 = b.multi(5,2);
        float multi2 = b.multi(5, 4, 3);

        System.out.println(sum1);
        System.out.println(sum2);
        System.out.println("Multiplication..");
        System.out.println(multi1);
        System.out.println(multi2);

    }
}
