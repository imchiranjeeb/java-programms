// Function Overloading
public class First {
    
    public int sum(int x, int y){
        return (x+y);
    }

    public int sum(int x, int y, int z){
        return (x+y+z);
    }

    public double sum(double x, double y, double z){
        return (x+y+z);
    }

    public static void main(String[] args){

        First f = new First();

        System.out.println(f.sum(5,6));
        System.out.println(f.sum(5,6,8));
        System.out.println(f.sum(5,6.8,5.6));

        var hh = f.sum(5,9);
        System.out.println(hh);
    }
}
