
class A{

    public int a;

    public int CTC(){
        return 30000;
    }

    public String jobID() {
        return "s-071";
    }

    public String address() {
        return "Bhuban";
    }

}


class B extends A{

    public String oldID() {
        return "w-01";
    }

    @Override
    public String address() {
        return "Bhubaneswar";
    }

}

public class Override {

    public static void main(String[] args){
        
        A a = new A();
        System.out.println(a.CTC());

        B b = new B();
        System.out.println(b.jobID());
        System.out.println(b.address());

    }
    
}
