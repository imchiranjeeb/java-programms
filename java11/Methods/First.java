
class First{

	int i = 5;
	static int j = 10;

	public static void show(){

		System.out.println("Calling Static Method.....");
		//System.out.println("i : " + i); // can not call non-static variable inside static method
		System.out.println("j : " + j);
	}

	public void disp(){
		System.out.println("Calling Non-Static Method.....");
		System.out.println("i : " + i);
		System.out.println("j : " + j);
	}

	public static void main(String[] args){

		First f = new First();

		show();

		f.disp(); // to call a non-static method class should be initialized

	}
}