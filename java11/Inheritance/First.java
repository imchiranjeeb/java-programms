class Bike{
    void speed(){
        System.out.println("Max Speed is 150");
    }

    void color(){
        System.out.println("Color is Blue.");
    }
}

class Human extends Bike{
    void bones(){
        System.out.println("They hv 320 bones");
    }
}

public class First {
    public static void main(String args[]){
        Human hm = new Human();
        hm.color();
    }
}
