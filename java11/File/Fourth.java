import java.io.File;

public class Fourth {
    public static void main(String[] args){
        try {

            String fileName = "test.txt";

            File newFile = new File(fileName);

            if(newFile.exists()){
                System.out.println("File name: " + newFile.getName());
                System.out.println("Absolute path: " + newFile.getAbsolutePath());
                System.out.println("Writeable: " + newFile.canWrite());
                System.out.println("Readable " + newFile.canRead());
                System.out.println("File size in bytes " + newFile.length());
            }else{
                System.out.println("File Does not exist");
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
