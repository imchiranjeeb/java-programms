import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


public class Second {
    public static void main(String[] args){
        try {

            String fileName = "test.txt";

            File newFile = new File(fileName);

            var isFileExists = newFile.exists();

            if(!isFileExists){
                newFile.createNewFile();
            }

            FileWriter myFileWriter = new FileWriter(fileName);
            myFileWriter.write("This will be written inside file....");
            myFileWriter.close();


        } catch (IOException e) {
            e.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
