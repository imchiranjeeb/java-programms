import java.io.File;
import java.util.Scanner;
import java.io.IOException;

public class Third {

    public static void main(String[] args){

        try {
            String fileName = "test.txt";

            File newFile = new File(fileName);


            if(!newFile.exists()){
                System.out.println(fileName + " does not exists.");
                newFile.createNewFile();
            }

            Scanner sc = new Scanner(newFile);

            while(sc.hasNextLine()){
                String line = sc.nextLine();
                System.out.println(line);
            }

            sc.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
