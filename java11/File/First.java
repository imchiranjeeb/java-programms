import java.io.File;
import java.io.IOException;

public class First{
    public static void main(String[] args){
        try {

            File newFile = new File("test.txt");

            var isFileCreated = newFile.createNewFile();

            System.out.println(isFileCreated);

            if(isFileCreated){
                System.out.println("File has been created.");
            }else{
                System.out.println("File Exists");
            }

        }catch(IOException e){
            System.out.println("Error Occured.");
            e.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}