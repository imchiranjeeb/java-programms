
class MyRouter{

    String routerName;
    int routerSerialno;

    public MyRouter(String name,int serialno) {
        this.routerName = name;
        this.routerSerialno = serialno;
    }

    public void showProperties(){
        System.out.println("Router Name is : " + this.routerName + " & Serial no is : " + this.routerSerialno);
    }

}

class MyNewRouter extends MyRouter {

    int routerPort;

    public MyNewRouter(String name,int serialno,int port) {
        super(name,serialno);
        this.routerPort = port;
    }

    public void showNewRouterProperty(){
        System.out.println("Router Name is : " + this.routerName + " & Serial no is : " + this.routerSerialno);
        System.out.println("Router port is : " + this.routerPort);
    }

}

public class Second {
    
    public static void main(String[] args){

        // MyRouter route1 = new MyRouter("CBK001",005);
        // route1.showProperties();

        MyNewRouter route2 = new MyNewRouter("CBK001",005,8080);
        route2.showNewRouterProperty();
    }
}
