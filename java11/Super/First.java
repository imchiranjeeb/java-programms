
//The super keyword in Java is used in subclasses to access superclass members (attributes, constructors and methods).
//It is used to call superclass methods, and to access the superclass constructor.
//To access attributes (fields) of the superclass if both superclass and subclass have attributes with the same name.

class Animal{

    // overridden method
    public void display(){
        System.out.println("This is Animal class . Display method.");
    }
}


class Dog extends Animal{

    // overridden method
    @Override
    public void display(){
        System.out.println("This is Dog class . Display method.");
    }

    public void printMessage(){
        // this calls overriding method
        display();
        // this calls overridden method
        super.display();
    }

}

public class First{

    public static void main(String[] args){

        Dog dog = new Dog();
        dog.printMessage();
    }
}