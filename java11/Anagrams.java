import java.util.*;

public class Anagrams {

    public static boolean isAnagrams(String a, String b){

        int aLength = a.length();
        int bLength = b.length();

        a = a.toLowerCase();

        b = b.toLowerCase();

        if(aLength != bLength) return false;

        int[] char_frequencies = new int[26];
        System.out.println(char_frequencies);

        for(int i=0; i<aLength; i++){
            
            int index = a.charAt(i) - 'a';
            char_frequencies[index]++;
            System.out.println(char_frequencies[index]);

        }

        for(int i=0; i<bLength; i++){

            int index = b.charAt(i) - 'a';
            char_frequencies[index]--;
            System.out.println(char_frequencies[index]);

        }

        for(int i=0; i<26; i++){
            if(char_frequencies[i] != 0) return false;
        }

        return true;
    }
    
    public static void main(String[] args){

        boolean isAnagram = isAnagrams("Cat","Act");

        if(isAnagram){
            System.out.println("Both the strings are anagrams.");
        }else{
            System.out.println("Not anagrams.");
        }

    }
}
//Systemstr=['hhh','ttt','kkk']
