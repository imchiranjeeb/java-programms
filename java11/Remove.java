import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


class Remove{


    public static String[] removeDuplicates(String[] arr){

        int pos=0;
        String[] resultantArray = new String[arr.length-1];

        for(int i=0; i<arr.length; i++){
            
            if(arr[i].equals("") || arr[i].equals("\n")){
                continue;
                // System.out.println(i);
            }
            resultantArray[pos] = arr[i];
            pos++;
            //System.out.println(pos);
            // System.out.println(arr[i]);
        }

        System.out.println(Arrays.toString(resultantArray));

        return resultantArray;
    }

    public static void removeDuplicates2(String[] arr){


        for(int i=0; i<arr.length; i++){

            if(arr[i].equals("") || arr[i].equals("\n")){

                for(int j=i; j<arr.length; j++){
                    arr[j] = arr[j+1];
                }
                break;

            }

        }

        System.out.println(Arrays.toString(arr));
    }

    public static void main(String[] args){

        String[] ele = {"assertion","","","","read-write","","","local","\n"};

        String[] ele2 = removeDuplicates(ele);

        System.out.println(ele2[2]);

        // for(String e:ele2){
        //     System.out.println(e);
        // }

    }
}