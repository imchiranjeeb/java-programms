public class Practise1{
    public static void main(String[] args){
        int h = 56, g= 45, y = 3, x = 5, o=3 , u=12;
        y *=y;
        int total = (h+g+y+x+o+u);
        ++total;
        o++;
        boolean res1 = total == g; //op:
        boolean res2 = y!=o; //op:
        boolean res3 = (y+o)>=x; //op:
        boolean res4 = (g+=15) >= (h+o); //op:
        boolean res5 = (g<50) && (y<x); //op:
        boolean res6 = (h+50>=103) || (u<y); //op:
        boolean res7 = !(!(res1)); //op:
        boolean res8 = !(!(!(res2))); //op:
        boolean res9 = !(res3); //op:
        boolean res10 = !(!(!(!(res4)))); //op:
        boolean res11 = res5 || res6; //op:
        boolean res12 = !(res8 && res10); //op:
        boolean res13 = !(!(res7 || res9)); //op:
        boolean res14 = !(!(!(res11 || res13))); //op:
        boolean res15 = !(!(!(!(res12 && res14)))); //op:
        boolean res16 = !(!(!(!(!(res15 && res14))))); //op:
        //
        System.out.println(!(!(!(!(!(!(res16))))))); //op:
    }
}