enum Size {
    SMALL,MEDIUM,LARGE,EXTRALARGE
}


class Test{

    Size size;

    public Test(Size pizzaSize) {
        this.size = pizzaSize;
    }

    public void orederPizza() {

        switch(size){

            case SMALL:
                System.out.println("SMALL Pizza.");
                break;
            case LARGE:
                System.out.println("LARGE Pizza.");
                break;
            default:
                System.out.println("Not in Option.");
                break;
        }
    }
}




public class Second {

    public static void main(String[] args) {

        Test t1 = new Test(Size.SMALL);
        t1.orederPizza();
    }
    
}
