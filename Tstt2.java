import java.util.*;

class HelloWorld {
    public static void main(String[] args) {
        List<String> all = new ArrayList<String>();
        ArrayList<HashMap<String, String>> allOPs = new ArrayList();
        
        all.add("acd:1");
        all.add("switch_address:172.16.3.4");
        all.add("switch_release:Communication:Mgr:8.x");
        all.add("link_type:TCP/IP");
        all.add("link_no:5001");
        
        all.add("switch_address:172.16.3.4");
        all.add("acd:2");
        all.add("switch_release:Communication:Mgr:8.x");
        all.add("link_no:5001");
        all.add("link_type:TCP/IP");
        
        all.add("switch_address:172.16.3.4");
        all.add("acd:3");
        all.add("switch_release:Communication:Mgr:8.x");
        all.add("link_no:5001");
        all.add("link_type:TCP/UDP");
        
        // System.out.println(all);
        int sizeInnerArray = all.size();
        int ttlSubarr = sizeInnerArray/5;
        int ttlArrr = sizeInnerArray/ttlSubarr;
        System.out.println("ttlArr " + ttlSubarr);
        
        for (int s = 0; s < all.size(); s += ttlArrr){
            
            int end = Math.min(s+ttlArrr,all.size());
            List<String> sublist = all.subList(s,end);
            HashMap<String, String> hm1 = new HashMap<>();
            
            for(int t=0; t<sublist.size(); t++){
                String currEle = sublist.get(t);
                if(currEle.startsWith("acd")){
                    hm1.put("acd_id",currEle.split(":")[1]);
                }
                if(currEle.startsWith("switch_address")){
                    hm1.put("acd_address",currEle.split(":")[1]);
                }
                if(currEle.startsWith("link_type")){
                    hm1.put("acd_link_protocol",currEle.split(":")[1]);
                }
                if(currEle.startsWith("link_no")){
                    hm1.put("acd_link_port",currEle.split(":")[1]);
                }
                if(currEle.startsWith("switch_release")){
                    String val = currEle.split(":",2)[1].replaceAll(":", " ");
                    hm1.put("acd_version",val);
                }
            }
            allOPs.add(hm1);
        }
        System.out.println("\n");
        System.out.println(allOPs);
    }
}
