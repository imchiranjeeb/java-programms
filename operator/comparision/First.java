public class First {
    public static void main(String[] args){
        int i = 5;
        int j = 5;
        System.out.println(i==j); //op:-true
        System.out.println(7==8);
        //Not equal
        int k = 10;
        int l = 11;
        int m = 10;
        System.out.println(k!=l);
        System.out.println(m!=k);
        //Greater than & Less than
        int o = 90;
        int p = 56;
        int q = 85; //> Greater than || < less than
        System.out.println(p>o);
        System.out.println(q<o);
        //Greater than or equal to(>=) || Less than or equal to(<=)
        int u = 257;
        int z = 261;
        int y = 275;
        int v = 275;
        System.out.println(z>=u);
        System.out.println(y<=u);
        System.out.println(v<=y);
        System.out.println(u>=v);
    }
}