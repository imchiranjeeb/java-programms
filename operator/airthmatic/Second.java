public class Second{
    public static void main(String[] args){
        int i = 8;
        // i++; //Post-Increment
        // ++i; // Pre-increment
        int m = i++; // i =9
       //int n = ++i; // i =10

        // m = 8 i = 10 n =10
        // i= 9 m =8
        System.out.println("Value of i is : " + i + " & Value of m is : " + m);

        int n = ++i; // i =10
        //i = 10 n =10

        System.out.println("Value of i is : " + i + " & Value of n is : " + n);
    }
}