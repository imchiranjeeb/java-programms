public class First{
    public static void main(String[] args){
        // Addition operator
        int i = 10 + 70;
        int j = i + 100;
        int k = i + j;

        int l = 28;
        int m = 5;
        double div = l/m;
        float div2 = (float)l/m;
        int mod = l % m;
        long r = Math.round((float)l/m);


        System.out.println(i);
        System.out.println(j);
        System.out.println(k);
        System.out.println(div);
        System.out.println(div2);
        System.out.println(mod);
        System.out.println(r);
        //Pow
        double p1 = Math.pow(5,2); // This means 5 to the power 2 i.e 5*5
        double p2 = Math.pow(5,6); // This means 5 to the power 6 i.e 5*5*5*5*5*5
        System.out.println(p1);
        System.out.println(p2);
    }
}