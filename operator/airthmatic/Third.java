public class Third{
    public static void main(String[] args){
        int i = 12;
        // i--; //Post-Decrement
        // --i; //Pre-Decrement
        int j = --i;   // j=11,i=11
        int k = i--; // k=11,i=10
        int m = k++; //m=11,k=12
        int l = --i; //l=9,i=9
        // i=9,j=11,k=12,l=9,m=11,
        int op = i+j+k+l+m;
        System.out.println("Value of j is " + j +" & value of i is " + i);
        System.out.println("Value of k is " + k +" & value of i is " + i);
        System.out.println("Value of op is " + op);
    }
}