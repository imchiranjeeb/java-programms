public class Logical1{
    public static void main(String[] args){
        int i = 5;
        int j = 8;
        System.out.println(i==5 && j<i); //op:-false
        System.out.println(j>10 && i>4); //op:-false
        System.out.println(j<897 && i<45); //op:-true
        System.out.println(j>30 && i>20); //op:-false
    }
}