public class Logical2{
    public static void main(String[] args){
        int i = 20;
        int j = 25;
        System.out.println(i>j || i>10); //op:-true
        System.out.println(j>i || j<100); //op:-true
        System.out.println(120<j || (i<j && j>13)); //op:-true
        System.out.println((111>12 || j==13) || (011>11 && j<i)); //op:-true
    }
}