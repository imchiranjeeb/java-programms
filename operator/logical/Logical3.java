public class Logical3{
    public static void main(String[] args){
        int i = 4;
        int j = 3;
        int k = 5;
        int l = 6;
        System.out.println(!(i!=j)); //op:-false
        System.out.println(!(i==100 || i>j)); //op:-false
        System.out.println(!(j<i && j==100)); //op:- true
        System.out.println(!(( i==5||j<i) && (101>100 && 999<1000-5))); //op:-true
        System.out.println(!(!(!((i>j++ && --i==j) || (--i<j && ++j==i))))); //op:-true
        System.out.println(!(!((!(1+l > 2+j++ || k<3+i--)) && (!(i/2==j || k< --j+5))))); //op:-false
    }
}