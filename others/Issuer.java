// Online Java Compiler
// Use this editor to write, compile and run your Java code online

class Issuer {
    
    public static String getFormattedIssuer(String str){
        
        str = str.replaceAll("\\s", "");
        String c = "";
        String cn = "";
        String ou = "";
        String o = "";
        
        
        String[] splittedStr = str.split(",");
        
        for(int i=0; i<splittedStr.length; i++){
            
            if(splittedStr[i].startsWith("C") && !splittedStr[i].startsWith("CN")){
                c=splittedStr[i];
            }
            
            if(splittedStr[i].startsWith("CN")){
                cn=splittedStr[i];
            }
            
            if(splittedStr[i].startsWith("O")&& !splittedStr[i].startsWith("OU")){
                o=splittedStr[i];
            }
            
            if(splittedStr[i].startsWith("OU")){
                ou=splittedStr[i];
            }
        }
        
        String finalStr = c + "," + o + "," + ou + "," +cn;
        
       return finalStr;
        
    }
    
    public static void main(String[] args) {
        
        String str = "CN=aes35.vislab.in-325218231-labUseOnly, OU=AEServices, O=Avaya, C=US";
        
        String res = getFormattedIssuer(str);
        System.out.println(res);
    
    }
}
