// Online Java Compiler
// Use this editor to write, compile and run your Java code online

class NumRev {
    public static void main(String[] args) {
       
       int num = 520;
       int temp = num;
       int reminder=0;
       int rev = 0;
       
       while(temp != 0){
           reminder = temp%10;
           rev = rev*10 + reminder;
           temp = temp/10;
        //   System.out.println(temp);
       }
       
      System.out.println(rev);
       
    }
}