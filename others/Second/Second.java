package second;

import first.*;

public class Second {
    
    public static void main(String[] args){
        First f = new First();
        f.test();
        System.out.println("Second class main method");
    }

}
