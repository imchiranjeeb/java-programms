
import java.util.*;

public class Fourth {

    public static String isLexicographic(String str1,String str2) {
        return str1.compareTo(str2)>0?"Yes":"No";
    }

    public static String Capitalize(String str){
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    public static void main(String[] args){

        Scanner scan = new Scanner(System.in);

        String A = scan.nextLine();
        String B = scan.nextLine();
        String finalOutput = Capitalize(A) + " " + Capitalize(B);

        System.out.println(A.length() + B.length());
        System.out.println(isLexicographic(A,B));
        System.out.println(finalOutput);


        scan.close();
    }
}
