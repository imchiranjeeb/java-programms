import java.util.*;
import java.io.*;

public class Third {

    public static void main(String[] args){

        Scanner scan = new Scanner(System.in);

        int[][] arr = new int[6][6];
        int max = Integer.MIN_VALUE;

        for (int i = 0; i<6; i++){
            for(int j=0; j<6; j++){
                arr[i][j] = scan.nextInt();
            }
        }

        for (int i = 0; i<4; i++){
            for(int j=0; j<4; j++){
                int tempSum = arr[i][j] + arr[i][j+1] + arr[i][j+2] + arr[i+1][j+1] + arr[i+2][j] + arr[i+2][j+1] + arr[i+2][j+2];

                if(tempSum>max){
                    max = tempSum;
                }
            }
        }

        System.out.println(max);

        scan.close();
    }
    
}
