import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;



public class First {

    private static final Scanner scanner = new Scanner(System.in);


    public static boolean isEven(int num){
        return num%2==0;
    }

    public static String finalResult(int num){

        boolean isEvenNumber = isEven(num);
        String result="Wired";

        if(!isEvenNumber) result="Weird";
        if(isEvenNumber && num>=2 && num <=5) result="Not Weird";
        if(isEvenNumber && num>=6 && num <=20) result="Wired";
        if(isEvenNumber && num>20) result="Not Weird";

        return result;
    }

    public static void main(String[] args) {
        int N = scanner.nextInt();
        
        System.out.println(finalResult(N));

        scanner.close();
    }
    
}
