import java.util.*;

public class Second {

    public static void main(String[] args){

        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();

        int[] myArray = new int[n];

        for(int i=0; i<n; i++) {
            int temp = scan.nextInt();

            myArray[i] = temp;
        }

        System.out.println(myArray);

        sc.close();
    }
    
}
