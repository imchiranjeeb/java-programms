package com.smarterhi.smgr.datacollector;

import java.util.UUID;

import com.smarterhi.connectionmanager.ConnectionType;
import com.smarterhi.smgr.parser.SMGRVersion;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smarterhi.smgr.connectionfactory.ConcreteFactory;
import com.smarterhi.smgr.parser.CommandType;
import com.smarterhi.connectionmanager.ConnectionManager;
import com.smarterhi.connectionmanager.Result;
import com.smarterhi.connectionmanager.ResultStatus;
import com.smarterhi.connectionmanager.connection.Connection;

public class CommandLineReader {

	private static final Logger log = LogManager.getLogger();
    private static CommandLineReader reader = null;
    public static CommandLineReader getReader() {
        if (reader == null) {
            reader = new CommandLineReader();
        }
        return reader;
    }
    private CommandLineReader() {
    }
    public void cleanUp() {
        ConnectionManager.getInstance().stop();
    }
    public Result collectData(UUID runId, String systemIPOrFQDN, String valueOf, String userName, String password,
                              String command, SMGRVersion version, CommandType commandType)
    {
        Connection c;
        log.info("Collecting data from system {}. username {}, command {}"
                , systemIPOrFQDN, userName, command);

        try{
            c = ConnectionManager.getInstance()
                    .getConnection(runId.toString(),"CONNECTION_TERM",systemIPOrFQDN,Integer.parseInt(valueOf)
                            ,userName,password,ConcreteFactory.getInstance(),commandType);
            log.info("C is : {}",c);

            log.info("Command type : {}",commandType);

            /*if condition to check for web or any other connection
            * if web then call login sequence other wise carry on with the usual operation*/
            if(commandType.toString().contains("URL")){
                Result result;
                if (c != null) {
                    log.info("-----------Calling the connect method---------");
                    result = c.connect(systemIPOrFQDN, Integer.parseInt(valueOf), userName,
                            password, ConnectionType.CONNECTION_TYPE_WEBSITE);
                    log.info("Result from login sequence ------ {}",result.getCommandResult());
                    if (result.getExecutionStatus() != ResultStatus.RESULT_SUCCESS) {
                        log.fatal("Unable to connect to server {}. reason {}", systemIPOrFQDN,
                                result.getExecutionStatus().toString());
                        throw new Exception(result.getCommandResult());
                    }
                }
            }
            log.info("-----------Calling the Execute method---------");
            Result commandResult = c.execute(command);
            c.releaseConnection();
            return commandResult;
        }catch (Exception ex){
            log.error("CollectData Stack Trace", ex);
            log.fatal("Couldnt collect data from system {} commad {} reason {}",
                    systemIPOrFQDN, command, ex.getMessage());
            return new Result(ResultStatus.RESULT_FAILURE,ex.getMessage());
        }
    }
	
}
