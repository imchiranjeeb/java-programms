
public class RemoveDigits{
    
    public static String removeDigits(String str){
        
        String result = "";
        
        for(int i=0; i<str.length(); i++){
            
            if(!Character.isDigit(str.charAt(i))){
                result += str.charAt(i);
            }
        }
        
        return result;
    }
    
    public static String removeDigits2(String str){
        return str.replaceAll("\\d","");
    }
    
    
	public static void main(String[] args) {
		
		System.out.println(removeDigits2("22HelloLooser002"));
	}
}
