// Online Java Compiler
// Use this editor to write, compile and run your Java code online

class ReverseArrayEle2 {
    public static void main(String[] args) {
        
        // char[] arr = {'H', 'e', 'l', 'l', 'o', '!'};
        int[] arr = {1,2,3,4,5,6,7,8,9};
        
        int start = 0,end=arr.length-1;
        
        while(start<end){
            int temp = arr[start];
            arr[start] = arr[end];
            arr[end] = temp;
            
            start++;
            end--;
        }
        
        for(int j=0; j<arr.length; j++){
            System.out.println(arr[j]);
        }
    }
}