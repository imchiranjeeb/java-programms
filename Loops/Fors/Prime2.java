import java.util.Scanner;
public class Prime2{
    public static void main(String[] args){
        Scanner ob = new Scanner(System.in);
        System.out.print("Enter a Number : ");
        int no = ob.nextInt();
        boolean isPrimeNumber = true;

        for(int i=2; i<no; i++){
            if (no%i==0){
                isPrimeNumber = false;
                break;
            }
        }

        if(isPrimeNumber){
            System.out.println(no + " is prime number.");
        }else{
            System.out.println( no + " is composite number.");
        }
    }
}