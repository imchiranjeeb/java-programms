import java.util.Scanner;
public class PrimeSerial{
    public static void main(String[] args){
        int start,end,i,j,temp=0;

        Scanner ob = new Scanner(System.in);
        System.out.print("Enter Starting Number: ");
        start = ob.nextInt();
        System.out.print("Enter End Number: ");
        end = ob.nextInt();

        for(i=start; i<=end; i++){

            for(j=2; j<i; j++){
                if(i%j==0){
                    temp = temp + 1;
                }
            }

            if(temp==0){
                System.out.println(i);
            }else{
                temp = 0;
            }
        }

    }
}