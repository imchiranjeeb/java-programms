import java.util.Scanner;
public class PrimeSerial2{
    public static void main(String[] args){
        int start,end,i,j;
        boolean primes = true;

        Scanner ob = new Scanner(System.in);
        System.out.print("Enter Starting Number: ");
        start = ob.nextInt();
        System.out.print("Enter End Number: ");
        end = ob.nextInt();

        for(i=start; i<=end; i++){

            for(j=2; j<i; j++){
                if(i%j==0){
                    primes = false;
                }
            }

            if(primes){
                System.out.println(i);
            }else{
                primes = true;
            }
        }

    }
}