//Given, N=80 then N+1 will be 80+1=81, which is a perfect square of the number 9. Hence 80 is a sunny number.
import java.util.Scanner;
public class Sunny{
    public static void main(String[] args){
        Scanner ob = new Scanner(System.in);
        System.out.print("Enter a Number :");
        int no = ob.nextInt(),j;
        boolean isSunny = false;

        for(int i=1;i<=no;i++){
            j = (i*i)-1;
            if(j==no){
                isSunny = true;
            }
        }

        if(isSunny){
            System.out.println(no + " is a sunny number");
        }else{
            System.out.println(no + " is not a sunny number");
        }
    }
}