import java.util.Scanner;

public class Fibonacci{
    public static void main(String[] args){
        Scanner ob = new Scanner(System.in);
        System.out.print("Enter the number of terms : ");

        int n,t1=0,t2=1,nextTerm;
        n = ob.nextInt();

        for(int i=1;i<=n;i++){
            System.out.println(t1);
            nextTerm = t1+t2;
            t1=t2;
            t2=nextTerm;
        }
    }
}