public class Evens{
    public static void main(String[] args){
        int i = 1,sum = 0;

        while (i<=10){
            if(i%2==0){
                System.out.println(i);
                sum += i;
            }
            i++;
        }
        System.out.println("Sum of Evens : " + sum);

 
    }
}