// 9(number) Neon :- 9*9 = 81(square), 8+1 = 9(sumOfSquare)
// 8 Neon :- 8*8 = 64, 6+4=10
import java.util.Scanner;

public class Neon{
    public static void main(String[] args){
        Scanner obj = new Scanner(System.in);
        int number,digit,square,sum=0;
        System.out.print("Enter a number: ");
        number = obj.nextInt();

        square = number*number;

        while(square != 0){
            digit = square % 10;
            sum = sum + digit;
            square = square/10;
        }

        if(number==sum){
            System.out.println(number + " is a neon number.");
        }else{
            System.out.println(number + " is not a neon number.");
        }
    }
}