import java.util.Scanner;
public class Duck2{
    public static void main(String[] args){
        Scanner ob = new Scanner(System.in);
        System.out.print("Enter a Number : ");
        int number = ob.nextInt(),reminder;
        boolean isDuckNumber = false;

        
        while(number>0){
            reminder = number%10;
            if(reminder==0){
                isDuckNumber = true;
            }
            number = number/10;
        }

        if(isDuckNumber){
            System.out.println("Duck Number.");
        }else{
            System.out.println("Not Duck Number");
        }
    }
}