import java.util.Scanner;
public class Prime{
    public static void main(String[] args){
        Scanner obj = new Scanner(System.in);
        System.out.print("Enter a Number Number ");

        int no = obj.nextInt(),i=2;
        boolean isPrime = true;

        if(no==1){
            System.out.println("This is neither prime nor composite.");
        }else if(no<1){
            System.out.println("Number can not be less than 1.");
        }else{

            while(i<no){
                if(no%i==0){
                    isPrime = false;
                    break;
                }
                i++;
            }
    
            if(isPrime){
                System.out.println( no + " is prime number." );
            }else{
                System.out.println( no + " is composite number." );
            }


        }

    }
}