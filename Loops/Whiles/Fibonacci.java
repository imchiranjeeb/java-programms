import java.util.Scanner;
public class Fibonacci{
    public static void main(String[] args){
        Scanner obj = new Scanner(System.in);
        System.out.print("Enter the Number of Terms : ");
        int i=1,n,t1=0,t2=1,nextTerm;
        n = obj.nextInt();

        while(i<=n){
            System.out.println(t1);
            nextTerm = t1 + t2;
            t1=t2;
            t2=nextTerm;
            i++;
        }
    }
}