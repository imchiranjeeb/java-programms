public class Reverse{
    public static void main(String[] args){
        int no = 121,rem,original,reverse=0;

        original = no;

        while(no != 0){
            rem = no%10;
            reverse =  reverse * 10 + rem;
            no = no/10;
        }

        if(original == reverse){
            System.out.println(original + " is a Palindrome Number.");
        }else{
            System.out.println(original + " is not a Palindrome Number.");
        }

    }
}