import java.util.Scanner;
public class Factorial{
    public static void main(String[] args){
        Scanner ob = new Scanner(System.in);
        System.out.print("Enter a Number : ");
        int no = ob.nextInt(),factorial=1,temp;

        temp = no;

        while(no > 0){
            factorial = factorial * no;
            no = no-1;
        }

        System.out.println("Factorial of " + temp + " is " + factorial);
    }
}