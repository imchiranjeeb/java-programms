public class SumFirstandLast{
    public static void main(String[] args){
        int num=1024,sum=0,firstDigit,lastDigit;

        lastDigit = num%10;

        firstDigit = num;

        while(num>=10){
            num = num / 10;
        }

        firstDigit = num;

        sum = firstDigit + lastDigit;

        System.out.println(sum);
    }
}