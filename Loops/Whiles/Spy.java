import java.util.Scanner;

public class Spy{
    public static void main(String[] args){
        Scanner ob = new Scanner(System.in);

        int num,product=1,sum=0,lastDigit;

        System.out.print("Enter a number : ");
        num = ob.nextInt();

        while(num>0){
            lastDigit = num%10;
            sum = sum + lastDigit;
            product = product * lastDigit;
            num = num/10;
        }

        if(sum==product){
            System.out.println("This is a Spy Number.");
        }else{
            System.out.println("This is not a Spy Number.");
        }
    }
}