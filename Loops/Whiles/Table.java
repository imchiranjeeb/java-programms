import java.util.Scanner;
public class Table{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int no = sc.nextInt(); //5
        int j = 1;

        while(j<=10){ //231 = 2+3+1 = 6
            System.out.println(no + " * " + j + " = " + (no*j));
            j++;
        }
    }
}

