public class Odds{
    public static void main(String[] args){
        int i = 10,sum=0;

        while(i>=1){
            if(i%2!=0){
                System.out.println(i);
                sum += i;
            }
            i--;
        }
        System.out.println("Sum of Odds : " + sum);

    }
}