//Narrowing Casting
public class Typecast2 {
    public static void main(String[] args){
        int i = 9;
        float f = (float)i;
        double d = (double)i;
        System.out.println(f);
        System.out.println(d);
    }
}