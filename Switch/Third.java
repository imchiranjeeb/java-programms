// day String value

// sunday 8am
// monday tuesday wednesday 6am
// Thursday 5am
// Friday Saturday 7am

public class Third{
    public static void main(String[] args){
        String day = "monday";
        String res;

        switch(day){
            case "sunday":
                res = "8am";
                break;
            case "monday":
            case "tuesday":
            case "wednesday":
                res = "6am";
                break;
            case "thursday":
                res = "5am";
                break;
            case "friday":
            case "saturday":
                res = "7am";
                break;
            default:
                res = "Day not Found";

        }

        System.out.println("I Wake up on " + day + " at " + res);
    }
}
