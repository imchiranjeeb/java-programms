public class First{
    public static void main(String[] args){
        String day = "Monday";
        String time;

        switch (day){
            case "Monday":
            case "Tuesday":
            case "Wednesday":
                time="7am";
                break;
            case "Thursday":
                time="5am";
                break;
            case "Friday":
                time="6am";
                break;
            case "Saturday":
                time="8am";
                break;
            case "Sunday":
                time="8am";
                break;
            default:
                time="day not found";
        }

        System.out.println(time);
    }
}